## Universidad Interamericana de Panamá
## Adrián Quintero

import random

class Tamagotchi:
	nombre = ""
	sexo = ""
	hambre = 10
	salud = 0
	felicidad = 10
	turno = 1
	
	def __init__(self, nombre, sexo):
		self.nombre = nombre
		self.sexo = sexo
		
	def cambiarTurno(self):
		self.turno += 1
		self.hambre -= 1
		self.felicidad -= 1
		if (self.felicidad > 10):
			self.felicidad = 10
		if (self.hambre > 10):
			self.salud += 2
	
	def alimentar(self):
		self.hambre += 2
		self.cambiarTurno()
	
	def jugar(self):
		self.felicidad += 3
		self.cambiarTurno()
		
	def hacerNada(self):
		self.cambiarTurno() 
	

def imprimirMedidores(tamagotchi):
	print("\n\n", tamagotchi.nombre)
	print("Sexo: ", tamagotchi.sexo)
	print("Turno: ", tamagotchi.turno)
	print ("Medidor de hambre: ", tamagotchi.hambre)
	print ("Medidor de salud: ", tamagotchi.salud)
	print ("Medidor de felicidad: ", tamagotchi.felicidad)
	
def terminarJuego(tamagotchi):
	imprimirMedidores(tamagotchi)
	print(f"{tamagotchi.nombre} ha muerto. El juego ha terminado.")

## Función principal ##
if __name__ == "__main__":
	sexos = ("Macho", "Hembra")
	nombre = input("Nombre de tu tamagotchi: ")
	tamagotchi = Tamagotchi(nombre, random.choice(sexos))
	
	while ((tamagotchi.salud < 10) and (tamagotchi.felicidad > 0) and (tamagotchi.hambre > 0)):
		imprimirMedidores(tamagotchi)
		print("""

Opciones:
[1] Alimentar
[2] Jugar
[3] Nada""")
		op = input(": ")
		if (op == "1"):
			tamagotchi.alimentar()
		elif (op == "2"):
			tamagotchi.jugar()
		elif (op == "3"):
			tamagotchi.hacerNada()
		elif (op == "-1"):
			break;
			terminarJuego(tamagotchi);
		else:
			print ("Opción inválida")
	terminarJuego(tamagotchi)


	
	
	
	
	
	